#ifndef __END_SCENE_H__
#define __END_SCENE_H__

#include "cocos2d.h"

class EndScene : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene(unsigned int tempScore);
	virtual bool init();

	// implement the "static create()" method manually
	CREATE_FUNC(EndScene);

private:
	cocos2d::Label* thisSessionScoreLabel;
	bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
	void drawScore();
};

#endif // __EndScene_SCENE_H__
