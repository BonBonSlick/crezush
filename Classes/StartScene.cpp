#include "StartScene.h"
#include "PlayScene.h"
#include "AudioEngine.h"

USING_NS_CC;
//using namespace std;


Scene* StartScene::createScene()
{
	return StartScene::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in StartScene.cpp\n");
}

// on "init" you need to initialize your instance
bool StartScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->setSwallowTouches(false);
	touchListener->onTouchBegan = CC_CALLBACK_2(StartScene::onTouchBegan, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);

	float circleRaius = Director::getInstance()->getVisibleSize().width / RandomHelper::random_int(12, 22);
	DrawNode* drawNode = DrawNode::create();
	drawNode->drawDot(
		Vec2(
			visibleSize.width / 2,
			visibleSize.height / 2
		),
		circleRaius,
		Color4F::RED
	);
	addChild(drawNode);

	return true;
}

bool StartScene::onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event)
{
	CCLOG(__FUNCTION__);
	//removeChildByTag(circleTagName);
//	AudioEngine::play2d("sounds/start.mp3");
	Director::getInstance()->replaceScene(
		TransitionFade::create(1, PlayScene::createScene())
	);
	return true;
}
