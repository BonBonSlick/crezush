#ifndef __HELLO_WORLD_SCENE_H__
#define __HELLO_WORLD_SCENE_H__

#include "cocos2d.h"

class PlayScene : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();

	// implement the "static create()" method manually
	CREATE_FUNC(PlayScene);

private:
	std::vector<cocos2d::Vec2> circleCenters;
	cocos2d::Color4F backgroundColor = cocos2d::Color4F(55 / 255.0, 55 / 255.0, 55 / 255.0, 1);
	cocos2d::DrawNode* pointDrawNode;
	int explostionParticles = 1;
	int minPointRadius = 12;
	float spawnPointSpeed = 2;
	float explosionDuration = 0.5;
	float explosionLife = 0.00001;
	float explosionSpeed = 100;
	float circleRaius;
	bool isCirclePressed = false;
	bool allowCircleGeneration = true;
	unsigned int score;
	cocos2d::Label* scoreLabel;
	enum  tagNames : unsigned char { line = 'l', point = 'p' } varName;
	cocos2d::Color4F circleColor;
	bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
	void spawnPoint(float dt);
	void removePoint();
	void removePoint(float dt);
	void drawField();
	void drawCircleNest(cocos2d::Vec2& position);
	void drawScore();
	void goToEndScene();
	void createExplosion();
	void update(float timeBetweenFrames);
};

#endif // __PlayScene_SCENE_H__
