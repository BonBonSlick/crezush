#include "EndScene.h"
#include "PlayScene.h"
#include "AudioEngine.h"

USING_NS_CC;
//using namespace std;


Scene* PlayScene::createScene()
{
	return PlayScene::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in PlayScene.cpp\n");
}

// on "init" you need to initialize your instance
bool PlayScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}
	AudioEngine::preload("sounds/hit.mp3");

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->setSwallowTouches(false);
	touchListener->onTouchBegan = CC_CALLBACK_2(PlayScene::onTouchBegan, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);

	float width = visibleSize.width;
	float height = visibleSize.height;
	float x = width / 3;
	float y = height / 3;
	float circleCenterX_1 = x / 2;
	float circleCenterY_1 = y / 2;
	circleCenters.push_back(Vec2(circleCenterX_1, circleCenterY_1));
	float circleCenterX_2 = x + circleCenterX_1;
	circleCenters.push_back(Vec2(circleCenterX_2, circleCenterY_1));
	float circleCenterX_3 = x * 2 + circleCenterX_1;
	circleCenters.push_back(Vec2(circleCenterX_3, circleCenterY_1));

	float circleCenterY_4 = y + circleCenterY_1;
	circleCenters.push_back(Vec2(circleCenterX_1, circleCenterY_4));
	float circleCenterY_5 = y + circleCenterY_1;
	circleCenters.push_back(Vec2(circleCenterX_2, circleCenterY_5));
	float circleCenterY_6 = y + circleCenterY_1;
	circleCenters.push_back(Vec2(circleCenterX_3, circleCenterY_6));

	float circleCenterY_7 = y * 2 + circleCenterY_1;
	circleCenters.push_back(Vec2(circleCenterX_1, circleCenterY_7));
	float circleCenterY_8 = y * 2 + circleCenterY_1;
	circleCenters.push_back(Vec2(circleCenterX_2, circleCenterY_8));
	float circleCenterY_9 = y * 2 + circleCenterY_1;
	circleCenters.push_back(Vec2(circleCenterX_3, circleCenterY_9));

	drawField();
	//spawnPoint(1);
	schedule(CC_SCHEDULE_SELECTOR(PlayScene::spawnPoint), spawnPointSpeed);
	scheduleUpdate();

	return true;
}

void PlayScene::drawField()
{
	for (int iterator = 0; iterator < circleCenters.size(); iterator++) {
		drawCircleNest(circleCenters.at(iterator));
	}
	auto lines = getChildByTag(1);
	DrawNode* drawLineNode = DrawNode::create();
	auto visibleSize = Director::getInstance()->getVisibleSize();
	float width = visibleSize.width;
	float height = visibleSize.height;
	float x = width / 3;
	float y = height / 3;
	Color4F& color = Color4F(1, 1, 1, 0.5);
	drawLineNode->drawLine(
		Vec2(x, height),
		Vec2(x, 0),
		color
	);
	drawLineNode->drawLine(
		Vec2(x + x, height),
		Vec2(x + x, 0),
		color
	);
	drawLineNode->drawLine(
		Vec2(0, y),
		Vec2(width, y),
		color
	);
	drawLineNode->drawLine(
		Vec2(0, y + y),
		Vec2(width, y + y),
		color
	);
	drawLineNode->setTag(tagNames::line);
	addChild(drawLineNode);
}

void PlayScene::drawCircleNest(cocos2d::Vec2& position)
{
	circleRaius = Director::getInstance()->getVisibleSize().width / minPointRadius;
	unsigned int segments = circleRaius * 2;
	DrawNode* drawNode = DrawNode::create();
	int  borderWidth = 1;
	drawNode->drawCircle(
		position,
		circleRaius + borderWidth,
		0,
		segments,
		false,
		Color4F(1, 1, 1, 0.5)
	);
	addChild(drawNode);

}

void PlayScene::drawScore()
{
	std::string scoreStringified = std::to_string(score);
	if (scoreLabel) {
		scoreLabel->setString(scoreStringified);
		return;
	}
	int fontSize = 15;
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	TTFConfig labelConfig;
	labelConfig.fontFilePath = "fonts/Marker Felt.ttf";
	labelConfig.fontSize = fontSize;
	labelConfig.glyphs = GlyphCollection::DYNAMIC;
	labelConfig.outlineSize = 2;
	labelConfig.customGlyphs = nullptr;
	labelConfig.distanceFieldEnabled = false;
	scoreLabel = Label::createWithTTF(
		labelConfig,
		scoreStringified,
		TextHAlignment::LEFT,
		visibleSize.height * fontSize
	);
	scoreLabel->setColor(Color3B::WHITE);
	scoreLabel->enableShadow(Color4B::BLACK);
	scoreLabel->enableOutline(Color4B::BLACK, 1);
	// position the label on the center of the screen
	scoreLabel->setPosition(
		Vec2(
			fontSize,
			visibleSize.height - fontSize
		)
	);
	// add the label as a child to this layer
	addChild(scoreLabel, 2);
}

void PlayScene::removePoint()
{
	unschedule(CC_SCHEDULE_SELECTOR(PlayScene::removePoint));

	if (!isCirclePressed) {
		goToEndScene();
	}
	score++;
	/*Node* pointNode = getChildByTag(tagNames::point);
	pointNode->setScale(0.1);*/
	//TransitionFade::create(1, EndScene::createScene(score));

	removeChildByTag(tagNames::point);
}

void PlayScene::removePoint(float dt)
{
	unschedule(CC_SCHEDULE_SELECTOR(PlayScene::removePoint));
	goToEndScene();
}

void PlayScene::spawnPoint(float dt)
{
	// @TODO - appear and disappear animation
	CCLOG(__FUNCTION__);
	isCirclePressed = false;
	if (!allowCircleGeneration) {
		return;
	}

	//circleRaius = Director::getInstance()->getVisibleSize().width / RandomHelper::random_int(minPointRadius, 22);
	circleRaius = Director::getInstance()->getVisibleSize().width / minPointRadius;
	explostionParticles = circleRaius * 2;

	int selectedVec2 = RandomHelper::random_int(0, static_cast<int>(circleCenters.size()) - 1);
	int minDarkColor = 25;
	double minOpacity = 0.1;

	Vec2 defaultPosition = Vec2(0, 0);
	circleColor = Color4F(
		RandomHelper::random_int(minDarkColor, 255) / 255.0,
		RandomHelper::random_int(minDarkColor, 255) / 255.0,
		RandomHelper::random_int(minDarkColor, 255) / 255.0,
		RandomHelper::random_real(minOpacity, 1.0)
	);

	pointDrawNode = DrawNode::create();
	//DrawNode* drawNode = DrawNode::create();
	pointDrawNode->setPosition(
		circleCenters.at(selectedVec2)
	);
	pointDrawNode->drawDot(
		defaultPosition,
		circleRaius,
		circleColor

	);
	pointDrawNode->setTag(tagNames::point);
	addChild(pointDrawNode);
	//Transition
	pointDrawNode->setScale(0.1);

	auto moveBy = ScaleBy::create(2, 10);
	auto moveByEaseOut = EaseOut::create(moveBy->clone(), 1);
	auto moveTo = ScaleBy::create(1, 0);
	auto moveToEaseIn = EaseIn::create(moveTo->clone(), 1);
	auto delay = DelayTime::create(1);
	auto seq = Sequence::create(moveByEaseOut, delay, moveToEaseIn, nullptr);
	seq->setTag(123);
	pointDrawNode->runAction(seq);

	//schedule(CC_SCHEDULE_SELECTOR(PlayScene::removePoint), spawnPointSpeed - 0.1);
}

bool PlayScene::onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event)
{
	CCLOG(__FUNCTION__);
	auto circleInner = getChildByTag(tagNames::point);
	if (!circleInner) {
		goToEndScene();
		return true;
	}

	auto height = Director::getInstance()->getOpenGLView()->getFrameSize().height;
	float distanceBetweenTouchAndCircleCenter = circleInner->getPosition().getDistance(
		Vec2(
			touch->getLocationInView().x,
			height - touch->getLocationInView().y
		)
	);

	isCirclePressed = distanceBetweenTouchAndCircleCenter <= circleRaius;

	CCLOG(isCirclePressed ? "true" : "false");

	if (true == isCirclePressed) {
		createExplosion();
		removePoint();
		drawScore();
	}
	else {
		goToEndScene();
	}

	return true;
}

void PlayScene::goToEndScene()
{
	allowCircleGeneration = false;
	//AudioEngine::play2d("sounds/end.mp3");
	Director::getInstance()->replaceScene(
		TransitionFade::create(1, EndScene::createScene(score))
	);
}

void PlayScene::createExplosion()
{
	AudioEngine::play2d("sounds/hit.mp3");

	auto circleInner = getChildByTag(tagNames::point);
	auto explode = ParticleExplosion::createWithTotalParticles(explostionParticles * 2);

	explode->setEndColor(Color4F(0, 0, 0, 0));
	explode->setGravity(Vec2(27.0, 25.0));
	explode->setLife(1);
	explode->setLifeVar(0.5);
	explode->setRadialAccel(125);
	explode->setRadialAccelVar(55);
	explode->setSpeed(255);
	explode->setSpeedVar(55);
	explode->setStartColor(circleColor);
	explode->setStartColorVar(circleColor);
	explode->setStartSize(25);
	explode->setStartSizeVar(5);
	explode->setEndSize(0);
	explode->setEndSizeVar(0);
	explode->setPosition(
		Vec2(
			circleInner->getPositionX(),
			circleInner->getPositionY()
		)
	);

	addChild(explode);
}

void PlayScene::update(float timeBetweenFrames)
{
	//if (pointDrawNode != nullptr && NULL != pointDrawNode && 0 == pointDrawNode->getNumberOfRunningActions()) {
	if (pointDrawNode != nullptr && NULL != pointDrawNode) {
		//if (pointDrawNode->getActionByTag(123) != nullptr) {
		schedule(CC_SCHEDULE_SELECTOR(PlayScene::removePoint), 1);
	}
}
