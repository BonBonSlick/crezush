#include "EndScene.h"
#include "PlayScene.h"
#include "AudioEngine.h"

USING_NS_CC;
//using namespace std;

unsigned int  thisSessionScore = 0;

Scene* EndScene::createScene(unsigned int tempScore)
{
	thisSessionScore = tempScore;

	return EndScene::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in EndScene.cpp\n");
}

// on "init" you need to initialize your instance
bool EndScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->setSwallowTouches(false);
	touchListener->onTouchBegan = CC_CALLBACK_2(EndScene::onTouchBegan, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);

	drawScore();

	float circleRaius = Director::getInstance()->getVisibleSize().width / RandomHelper::random_int(12, 22);
	DrawNode* drawNode = DrawNode::create();
	drawNode->drawDot(
		Vec2(
			visibleSize.width / 2,
			visibleSize.height / 3
		),
		circleRaius,
		Color4F::RED
	);
	this->addChild(drawNode);

	return true;
}

void EndScene::drawScore()
{
	int fontSize = 35;
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	UserDefault* def = UserDefault::getInstance();
	auto highScore = def->getIntegerForKey("HIGHSCORE", 0);
	if (thisSessionScore > highScore) {
		highScore = thisSessionScore;
		def->setIntegerForKey("HIGHSCORE", highScore);
	}
	def->flush();

	float currentSessionScoreTextYAxis = visibleSize.height - fontSize;
	float bestScoreTextXAxis = visibleSize.height - fontSize * 3;

	TTFConfig labelConfig;
	labelConfig.fontFilePath = "fonts/Marker Felt.ttf";
	labelConfig.fontSize = fontSize;

	thisSessionScoreLabel = Label::createWithTTF(
		labelConfig,
		std::to_string(thisSessionScore),
		TextHAlignment::RIGHT,
		visibleSize.height * fontSize
	);
	thisSessionScoreLabel->setColor(Color3B::WHITE);
	thisSessionScoreLabel->setPosition(
		Vec2(
			visibleSize.width - fontSize,
			currentSessionScoreTextYAxis
		)
	);
	thisSessionScoreLabel->setAnchorPoint(Vec2(1, 0.5));
	addChild(thisSessionScoreLabel, 3);

	auto thisSessionScoreLabelText = Label::createWithTTF(
		labelConfig,
		"CURRENT: ",
		TextHAlignment::LEFT,
		visibleSize.height * fontSize
	);
	thisSessionScoreLabelText->setColor(Color3B::WHITE);
	thisSessionScoreLabelText->setPosition(
		Vec2(
			fontSize,
			currentSessionScoreTextYAxis
		)
	);
	thisSessionScoreLabelText->setAnchorPoint(Vec2(0, 0.5));
	addChild(thisSessionScoreLabelText, 3);


	auto highScoreLabel = Label::createWithTTF(
		labelConfig,
		std::to_string(highScore),
		TextHAlignment::RIGHT,
		visibleSize.height * fontSize
	);
	highScoreLabel->setColor(Color3B::YELLOW);
	highScoreLabel->setPosition(
		Vec2(
			visibleSize.width - fontSize,
			bestScoreTextXAxis
		)
	);
	thisSessionScoreLabel->setAnchorPoint(Vec2(1, 0.5));
	addChild(highScoreLabel, 3);

	auto highScoreLabelText = Label::createWithTTF(
		labelConfig,
		"BEST: ",
		TextHAlignment::LEFT,
		visibleSize.height * fontSize
	);
	highScoreLabelText->setColor(Color3B::YELLOW);
	highScoreLabelText->setPosition(
		Vec2(
			fontSize,
			bestScoreTextXAxis
		)
	);
	highScoreLabelText->setAnchorPoint(Vec2(0, 0.5));
	addChild(highScoreLabelText, 3);

}

bool EndScene::onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event)
{
	CCLOG(__FUNCTION__);
	//AudioEngine::play2d("sounds/start.mp3");
	Director::getInstance()->replaceScene(
		TransitionFade::create(1, PlayScene::createScene())
	);

	return true;
}
